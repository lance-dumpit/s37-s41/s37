const express = require("express");
const userController = require("../controllers/userController");
const auth = require("../auth"); 
const router = express.Router();


// Route for User Registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(
		resultFromController));

});


// Routes for User Authentication
router.post("/login", (req,res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})



// Allows us to export the "router" obejct that will be accessed
module.exports = router