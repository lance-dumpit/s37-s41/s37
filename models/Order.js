const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	name: {
		totalAmount: Number,
		required: [true, "Total Amount is required."]
		},
		purchasedOn : {
				type : Date,
				default : new Date()
		}
		
		purchasers: [
			{
				userId: {
					type: String,
					required: [true, "UserId is required"]
				}
			}
		],

		purchases: [
			{
				productId: {
					type: String,
					required: [true, "UserId is required"]
				}
			}
		]

		
})

module.exports = mongoose.model("Order", orderSchema);