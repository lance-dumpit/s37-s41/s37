const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	name: {
		name: String,
		required: [true, "Product Name is required."]
		},
		description: {
			type: String,
			required: [true, "Description is required"]
		},
		price: {
			type: Number,
			required: [true, "Price is required"]
		},
		isActive: {
			type: Boolean,
			default: true
		}
		
})

module.exports = mongoose.model("Product", productSchema);